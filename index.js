// CRUD Operations
/*
    - CRUD operations are the heart of any backend application.
    - Mastering the CRUD operations is essential for any developer.
    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/

//[SECTION] Inserting Documents (CREATE)

/*
    Syntax:
        - db.colleactionName.insertOne({object});
*/


// Insert One
// whenever we insert document/s we are already creating a collection, however, a collection can already be created once.

db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "8765 4321",
        email: "janedoe@mail.com"
    },
    courses: ["CSS", "JavaScript", "Python"],
    department: "none"
});

// insert many

db.users.insertMany([
    {
    firstName: "Stephen",
    lastName: "Hawking",
    age: 76,
    contact: {
        phone: "87000",
        email:"stephenhawking@mail.com"
    },
    courses: ["Python", "React", "PHP"],
    department: "none"
},

{
    firstName: "Neil",
    lastName: "Armstrong",
    age: 82,
    contact: {
        phone: "87894561",
        email:"neilarmstrong@mail.com"
    },
    courses: ["React", "Laravel", "MongoDB"],
    department: "none"
}
]);

//[SECTION] Finding Documents (READ)

/*
    - db.collectionName.find();
    - db.collectionName.find({ field: value});
*/
db.users.find();
db.users.find({ firstName: "Stephen"});

db.users.find({lastName: "Armstrong", age: 82});

//
db.users.deleteOne({
    firstName: "Stephen"
})

//[SECTION] Updating Documents (UPDATE)
db.users.updateOne(
    {firstName: "Test"},
    {
        $set :{
            firstName : "Bill",
            lastName : "Gates",
            age : 65,
            contact : {
                phone: "12345678",
                email: "bill@mail.com"
            },
            courses: ["PHP", "Laravel", "HTML"],
            department: "operations",
            status : "active"
        } 
});

db.user.find({firstName : "Bill"});

//Updating multiple documents
/*
    Syntax:
        -db.colletionName.updateMany({criteria}, {$set {field : value}});
*/

db.users.updateMany(
    {department : "none"},
    {
        $set: {department: "HR"}
    }
);

//Replace One
/*
    -Can be used if replacing the whole document is necessary
*/

/*
    Syntax:
        db.colletionName.updateMany(
            {criteria},
            {field : value}
        );
*/

db.users.replaceOne(
    {firstName : "Bill"},
    {
        firstName : "Billy",
        lastName : "Crawford",
        age : 30,
        contact : {
            phone: "12345678",
            email: "billy@mail.com"
        },
        courses: ["PHP", "Laravel", "HTML"],
        department: "operations",
        status : "active"
    }
);

db.users.deleteOne({
    firstName : "Jane"
});

db.users.deleteMany({
    firstName : "Jane"
});